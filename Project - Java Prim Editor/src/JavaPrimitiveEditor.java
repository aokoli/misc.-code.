import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.*;

public class JavaPrimitiveEditor {
	
	public static void main(String[] args) throws FileNotFoundException {
		new JavaPrimitiveEditor().run();
	}
	
	List<String> doc = new LinkedList<String>();
	Set<String> dictionary = new HashSet<String>();
	String DocumentName;
	boolean saved = false;

	
	public void run() throws FileNotFoundException{
		
		final String DOC_FILEPATH_PROMPT = "Welcome to the Alex Java Primitive Editor.\n\n" +
									       "Please enter the path to the file to be edited: ";  
		
		final String DICTIONARY_FILEPATH_PROMPT = "Please enter the path to the dictionary: ";	
		
		final String X = "Please enter string X: ";
		
		final String Y = "Please enter string Y: ";
		
		FileReader(DOC_FILEPATH_PROMPT, doc, "N");
		FileReader(DICTIONARY_FILEPATH_PROMPT, dictionary, "Y");			

		boolean quit = false;   
		
		while(!quit){
			Bounderies();			
			Menu();
			
			String input = new Scanner(System.in).next();
			
			switch (input) {
				
				case "1" :  Find(Input(X)); 									   break;
				case "2" :  FindReplace(Input(X), Input(Y)); 				       break;
				case "3" :  FindInsert(Input(X), Input(Y));  				   	   break;
				case "4" :  Delete(Input(X)); 								       break;
				case "5" :  SpellCheck(); 						 				   break;
				case "6" :  SpellCheckAll();								   	   break;
				case "7" :  SaveAs(); 							 				   break;
				case "8" :  Save();												   break;
				case "9" :  Print();							 				   break;
				case "10":  FindReplaceAll(Input(X), Input(Y)); 			       break;
				case "q" :  case "Q": System.out.println("Goodbye!"); quit = true; break;
	            default  : System.out.println("Invalid input!");
	            
	        }
		}	
	}

	public boolean Find(String target){
		
		Iterator<String> itr = doc.iterator();
		String targetFTD = target.toLowerCase().trim(); // Target string is formatted (FTD) using lowercase and trim for comparison purposes below 
	
		while(itr.hasNext()){			
			String indexString = itr.next();
			String indexStringFTD = indexString.toLowerCase().trim(); // indexString is formatted (FTD) To compare with targetFTD
			
			StringBuilder text = new StringBuilder(indexString);	
			StringBuilder textFTD = new StringBuilder(indexStringFTD);	
			
	    	int start = textFTD.indexOf(targetFTD);	// Essentially the comparison of indexString and target string
	    	
	    	if (start != -1){  		    		
	    		System.out.println("\"" + target + "\"" + " was found."); 
		    	return true;		    		
	    	}
		
		}

		System.out.println("\"" + target + "\"" + " doesn't exist.");   
		return false;
	}
	
	
	public boolean FindReplace(String target, String replacement) throws FileNotFoundException{	

		Iterator<String> itr = doc.iterator();
		String targetFTD = target.toLowerCase().trim(); 
		int i = 0;
		
		while(itr.hasNext()){		
			String indexString = itr.next();
			String indexStringFTD = indexString.toLowerCase().trim(); 
			
			StringBuilder text = new StringBuilder(indexString);	
			StringBuilder textFTD = new StringBuilder(indexStringFTD);	
			
	    	int start = textFTD.indexOf(targetFTD);	
	    	   	
	    	if (start != -1){

		    	int end = target.length() + start;	
		    	String replacedIndex = new String (text.replace(start, end, replacement));	
		    	doc.set(i, replacedIndex);
		    	System.out.println("\"" + target + "\"" +  " was replaced with " + "\"" + replacement + "\"");    
		    	return true;
	    	}
			i++;
		}		
		System.out.println("\"" + target + "\"" + " doesn't exist.");  
		return false;
	}
	
	
	public boolean FindReplaceAll(String target, String replacement){
		
		Iterator<String> itr = doc.iterator();
		String targetFTD = target.toLowerCase().trim(); 
		int i = 0;
		boolean hasReplaced = false;
		
		while(itr.hasNext()){		
			String indexString = itr.next();
			String indexStringFTD = indexString.toLowerCase().trim(); 
			
			StringBuilder text = new StringBuilder(indexString);	
			StringBuilder textFTD = new StringBuilder(indexStringFTD);	
			
	    	int start = textFTD.indexOf(targetFTD);		
	    	   	
	    	if (start != -1){
	    		
    			hasReplaced = true;
		    	int end = target.length() + start;	
		    	String replacedIndex = new String (text.replace(start, end, replacement));	
		    	doc.set(i, replacedIndex);
	    		
	    	}
			i++;
		}
		if (hasReplaced == true){
			System.out.println("\"" + target + "\""  + " was replaced throughout the document with " + "\"" + replacement + "\"" );  
			return true;
		} else {
			System.out.println("\"" + target + "\"" + " doesn't exist.");  
			return false;
		}	
	}
	
	
	public boolean FindInsert(String target, String insert){
		
		Iterator<String> itr = doc.iterator();
		String targetFTD = target.toLowerCase().trim(); 
		int i = 0;
		
		while(itr.hasNext()){		
			String indexString = itr.next();
			String indexStringFTD = indexString.toLowerCase().trim(); 
			
			StringBuilder text = new StringBuilder(indexString);	
			StringBuilder textFTD = new StringBuilder(indexStringFTD);	
			
	    	int start = textFTD.indexOf(targetFTD);	
	    	
	    	if (start != -1){
	    		int end = target.length() + start;	
		    	
		    	if (end == indexString.length()){
		    		doc.add(i + 1, insert);  
		    		System.out.println("Insertion complete!");
		    		return true;		    		
		    	}
		    	String replacedIndex = new String (text.insert(end, insert));	
		    	doc.set(i, replacedIndex);
		    	System.out.println("Insertion complete!");
		    	return true;		    		 		
	    	}
			i++;
		}		
		System.out.println("\"" + target + "\"" + " doesn't exist.");
		return false;
	}
	
	
	public boolean Delete(String target){

		Iterator<String> itr = doc.iterator();
		String targetFTD = target.toLowerCase().trim(); 
		int i = 0;
		
		while(itr.hasNext()){		
			String indexString = itr.next();
			String indexStringFTD = indexString.toLowerCase().trim(); 
			
			StringBuilder text = new StringBuilder(indexString);	
			StringBuilder textFTD = new StringBuilder(indexStringFTD);	
			
	    	int start = textFTD.indexOf(targetFTD);	
	    	
	    	if (start != -1){	
    			int end = target.length() + start;	
	    		
	    		if (end == indexString.length()){
		    		doc.remove(i); 
		    		System.out.println("\"" + target + "\"" + " was deleted.");
		    		return true;		    		
		    	}
	    		
		    	String replacedIndex = new String (text.replace(start, end, ""));	
		    	doc.set(i, replacedIndex);
	    		System.out.println("\"" + target + "\"" + " was deleted.");
		    	return true;
	    	}
			i++;
		}		
		System.out.println("\"" + target + "\"" + " doesn't exist.");
		return false;
	}
	
	
	public void Print(){
		
		int count = 0;		
		System.out.println("***DOCUMENT BEGINNING***");
		System.out.println();
		
		for (String index : doc){
			count++;
			System.out.print(index + " ");
			if (count % 20 == 0){
				System.out.println();
			}
		}
		System.out.println("\n");
		System.out.println("***DOCUMENT END***");
	}
	
	
	public void SaveAs() throws FileNotFoundException{
		System.out.println("Please enter the name you want to save the document as (e.g. \"JPFileEditorOutput\"): ");
		String input = new Scanner (System.in).next(); 
		DocumentName = input + ".txt";
		PrintStream output = new PrintStream(new File (DocumentName));
		
		int count = 0;
		for (String index : doc){
			count++;
			output.print(index + " ");
			if (count % 20 == 0){
				output.println();
			}
		}
		
		System.out.println("Your file has been saved as " + DocumentName);
		output.close();
		saved = true;
		
	}
	
	
	public void Save() throws FileNotFoundException{
		if (saved == false){
			SaveAs();
		} else {
			PrintStream output = new PrintStream(new File (DocumentName));
			
			int count = 0;
			for (String index : doc){
				count++;
				output.print(index + " ");
				if (count % 20 == 0){
					output.println();
				}
			}
			
			System.out.println(DocumentName + " has been saved.");
			output.close();
		}
	}
	
	
	public String SpellCheck(){
	
		Iterator<String> itr = doc.iterator();
		
		while (itr.hasNext()){
			String word = itr.next();
			String wordFTD = word.replaceAll("[\\[\\]:0-9()-/.,*!?\";��'`]", "").toLowerCase().trim(); 
			
			if (!dictionary.contains(wordFTD)){
				System.out.println("First misspelled word: " + word);  
				return word;
			}	
		}

		System.out.println("Spell Check Passed!");  
		return "Spell Check Passed!";
	}

	
	public void SpellCheckAll(){
		
		Iterator<String> itr = doc.iterator();
		boolean hasMisspelling = false;
		Set<String> allMisspelledWords = new TreeSet<String>();
		int i = 0;
		
		System.out.println("Processing (formatting results)...");
		while (itr.hasNext()){
			String word = itr.next();
			String wordFTD = word.replaceAll("[\\[\\]:0-9()-/.,*!?\";��'`]", "").toLowerCase().trim(); 			
			
			if (!dictionary.contains(wordFTD)){
				allMisspelledWords.add(word);
				hasMisspelling = true;
			}
			
		}
		System.out.println();
		System.out.println("Processing Complete.\n");
		
		if (!hasMisspelling) {
			System.out.println("Spell Check Passed!");
		} else {
			System.out.println("Misspelled word(s) (Ignore punctuation): " + allMisspelledWords);
		}
	}
	
	
	public static void FileReader(String filePathPrompt, Collection<String> container, String format) throws FileNotFoundException{
		System.out.println(filePathPrompt);
		String filePath = new Scanner (System.in).nextLine(); 			
		Scanner sc = new Scanner(new File(filePath)); 
		
		if (format.equals("N")){
			while (sc.hasNext()){
				container.add(sc.next());			
			}
			sc.close();	
		}
		
		if (format.equals("Y")){
			while (sc.hasNext()){
				container.add(sc.next().replaceAll("[\\[\\]\\-\\:0-9()-/.,*!?\";��'`]", "").toLowerCase().trim()); 	
			}
			sc.close();	
		}
		
	}

	
	public static void Menu(){
		System.out.println("Please follow the instructions below to manipulate the document:\n" +
				   "Press 1 for Find(String X) - Finds the first occurrence of X.\n" +
				   "Press 2 for FindReplace(String X, String Y) - Finds first occurence of X replaces it with Y.\n" +
				   "Press 3 for FindInsert(String X, String Y) - Finds X and inserts Y in front of it.\n" + 
				   "Press 4 for Delete(String X) - Deletes the first occurrence of X.\n" +
				   "Press 5 for SpellCheck() - Checks if all words are spelled accurately.\n" + 
				   "Press 6 for SpellCheckAll() - Prints all the misspelled words to the screen.\n" +
				   "Press 7 for SaveAs() - Saves the document under a user-entered file name.\n" + 
				   "Press 8 for Save() - Saves the document.\n" +
				   "Press 9 for Print() - Prints the document to the console window.\n" +
				   "Press 10 for FindReplaceAll(String X, String Y) - Finds all occurrences of X and replaces them with Y.\n" +
				   "Press \"Q\" to quit program.");		
	}
	
	
	public String Input(String str){
		System.out.println(str);			
		String input = new Scanner (System.in).nextLine(); 
		return input;				
	}	
	
	public void Bounderies(){
		System.out.println("\n");
		for(int i = 0; i < 150; i++){
			System.out.print("*");
		}
		System.out.println("\n");
	}
	

}
