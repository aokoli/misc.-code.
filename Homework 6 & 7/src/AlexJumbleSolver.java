import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
// C:\Users\Somto\Downloads\EnglishWordList3.txt

public class AlexJumbleSolver {

	public static void main(String[] args) throws FileNotFoundException {		
		run();		
	}
	
	public static void run() throws FileNotFoundException {
		
		System.out.println("Please enter the full file path for dictionary file: ");		
		String fileName = new Scanner (System.in).nextLine(); 		
		System.out.println("Reading dictionary file...");
		
		Map<String, List<String>> sortedDictionary = new HashMap<String, List<String>>();
		Scanner dictionary = new Scanner(new File(fileName)); 
		
		while(dictionary.hasNext()){
			
			String original = dictionary.next();
			String sorted = sort(original);
			sorted.toLowerCase(); // sorted.replaceAll("[!-?]", "").toLowerCase().trim();  OR replaceAll("[:0-9()-/.,*!?\";'��']", "")" <-- Both costly!
	        
			List<String> dictValues = new ArrayList<String>();       
	        
	        if (sortedDictionary.containsKey(sorted)){
	        	dictValues = sortedDictionary.get(sorted);
	        	dictValues.add(original);
	        	sortedDictionary.put(sorted, dictValues);
	        } else {
	        	
	        	dictValues.add(original);
	        	sortedDictionary.put(sorted, dictValues);
	        }     

		}
		
		dictionary.close();
		boolean quit = false;
		
		System.out.println("Reading complete.\n");
		
		while(!quit){
			System.out.println("Enter jumbled word (To quit, type \"Q\" and then press Enter): ");
			Scanner sc = new Scanner(System.in);
			String input = sc.next().toLowerCase();
			
			if (input.equals("q")) {
				System.out.println("Thanks for using AlexJumbleSolver! Goodbye!");
				sc.close();
				quit = true;
			} else {
				Set<String> foundWords = new TreeSet<String>();				
				String sorted = sort(input);
				
		        if (sortedDictionary.get(sorted) != null){		        
		        	System.out.println("The unjumbled words (of string length) are: " + sortedDictionary.get(sorted));
		        } else {
		        	System.out.println("There are no unjumbled words (of string length).");
		        }		        
		        
		        System.out.println("The complete list of formable words from string are below (this might take a while to display):");	
				System.out.println(wordSolver(input, new StringBuffer(), 0, sortedDictionary, foundWords) + "\n");
			}
			
		}

	}
	

	
	static Set<String> wordSolver(String jumble, StringBuffer combination, int index, Map<String, List<String>> sortedDictionary, Set<String> foundWords)
	{
	    for (int i = index; i < jumble.length(); i++)
	    {
	        combination.append(jumble.charAt(i));	        
	        String sorted = sort(new String(combination));
	        
	        if (sortedDictionary.get(sorted) != null){	        
	        	foundWords.addAll(sortedDictionary.get(sorted));      	        	      	
	        }    
	        
	        wordSolver(jumble, combination, i + 1, sortedDictionary, foundWords);	        
	        combination.deleteCharAt(combination.length() - 1);  
	    }   
	    
	    return foundWords;
	} 
	
	
	
	public static String sort(String str){
		String original = str;
		char[] chars = original.toCharArray();
		Arrays.sort(chars);
		return new String(chars);
	}

} 