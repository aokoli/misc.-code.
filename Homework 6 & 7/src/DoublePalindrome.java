/*
 * Program checks if input string is a double palindrome or not.
 */

import java.util.Scanner;

public class DoublePalindrome {

	public static void main(String[] args) {
		run();		
	}
	
	public static void run (){
		final String INPUT_PROMPT = "Please enter a string with no whitespaces (Press \"Q\" to quit): ";
		
		boolean quit = false;
		
		while(!quit){
			System.out.println(INPUT_PROMPT);		
			
			String str = new Scanner(System.in).nextLine();
			
			if (str.equals("q") || str.equals("Q")){
				System.out.println("Goodbye!");
				quit = true;
			} else {		
				if (isDoublePalindrome(str)){
					System.out.println(str + " is a double palindrome!");
				} else {
					System.out.println(str + " is NOT a double palindrome.");
				}
			}
		}
	}
	
	public static boolean isPalindrome(String string){
		String reverseString = new StringBuffer(string).reverse().toString();
		return string.equalsIgnoreCase(reverseString);
	}
	
	
	public static boolean isDoublePalindrome(String string){		
		if (string.length()%2 != 0 || string.length() <= 0){  // A double palindrome should ALWAYS be even! (and a non-blank String)
			return false;
		}
		String halfString = string.substring(0, (string.length()/2));
		
		return (isPalindrome(halfString) && isPalindrome(string));
	}
	

}
