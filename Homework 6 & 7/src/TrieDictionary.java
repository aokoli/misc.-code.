import java.util.*;

public class TrieDictionary {

	public class Node {
		// Parent is key, child(ren) is value.
		// Map<Character, ArrayList<Node>> 
		
		Character parent;
		Character child;
		Map<Character, Node> children;
		boolean terminalNode;
		Node next;
		
		
		public Node(Character parent){
			this.parent = parent;
			this.child = null;
			this.children = null;
			this.next = null;
		}
		
		
		public void addNode(Character parent){
			this.parent = parent;
		//	this.child = child;
		//	this.children.add(child);
		}
		
		public void addChild(Map<Character, Node> child){
			this.children.putAll(child);
		}
		
		public Map<Character, Node> getChildren(){
			return children;
		}
		
		public void setTerminalNode(boolean verdict){
			this.terminalNode = verdict;
		}		
		
	}
	
	// Adding words/nodes to trie dictionary
	public void addWord(){
		
		// Create head nodes
		Map<Character, Node> headNodes = new HashMap<Character, Node>();
		final String alphabets = "abcdefghijklmnopqrstuvwxyz";		
		char[] firstLetters = alphabets.toCharArray();
		for (char letter : firstLetters){
			headNodes.put(letter, null);
		}
		
		// check each char in string as head node, and use remaining chars are children tests
		String word = "nasty";   // For each char, treat like HEAD and CHILD
		char[] letters = word.toCharArray();
		ArrayList<Character> availableLetters = new ArrayList<Character>();
		int aLSize = letters.length;
		if (aLSize >= 1){		
			for (int i = 1; i < aLSize; i++){
				availableLetters.add(letters[i]);   // available letters "a,s,t,y"
			}
		}
		char currentParentLetter = word.charAt(0);   // 'n'
		Node currentNode = headNodes.get(currentParentLetter);  // map<'n', null>

		// connecting links in trie with head node
		for (int i = 0; i < availableLetters.size(); i++){			
			System.out.println(availableLetters.get(i));

			Map<Character, Node> currentChildren = currentNode.getChildren(); // n --> null, null | a --> null, null			
			if (currentChildren.containsKey(availableLetters.get(i))){ // if contains 'a'
				currentNode = currentChildren.get(availableLetters.get(i)); // currentNode = n -> a 
			} else {
				Map<Character, Node> child = new HashMap<Character, Node>();
				child.put(availableLetters.get(i), null);
				currentNode.addChild(child);  // creates connection n -> a
				currentChildren = currentNode.getChildren();  // gets n children, a, s, t, y
				currentNode = currentChildren.get(availableLetters.get(i)); // gets a node
			}
			// currentParentLetter = availableLetters.get(i);  // makes current parent letter = a
			
			
			
			
			/*
			//do something recursive with availableletters and return string
			for (int j = 0; j < availableLetters.size(); j++){
				if (currentChildren.containsKey(availableLetters.get(j))){ // if contains 'a'
					currentNode = currentChildren.get(availableLetters.get(j)); // currentNode = n -> a 
				} else {
					currentNode.put();
				}
			}
			*/
		
		}
		
		
		// Check for all children of head contained in word
		for (int i = 0; i < word.length(); i++){
			

			// Search for "n" header  (Map<Character, Set<Character>>
			// Add n
			// Track n with 'path' variable (if any char is added to node, add to path)
			// if "a" isn't child of "n", add "a" etc.
			// If a is child, skip to next char ("s"). (use getCurrentNode() to keep track) 
			// Check if 's' is child of 'a' (n --> a --> s...)
			// At final word index, make terminal true in node
			
			/*
			trieDictionary.addNode(word.charAt(i));  // n added
			previousNode.isChild(word.charAt(i));    // if a is child of n
			*/
		}
	
	}
	

	
	
	
	
	
	
	public static void main(String[] args) {
		TrieDictionary test = new TrieDictionary();
		test.addWord();

	}
	
	
	

}
