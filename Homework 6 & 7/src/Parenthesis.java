/*
 * The program checks if parenthesis have the correct ordering.
 * 
 * The time complexity of the program is O(n) as each character in 
 * the input String would have to be accessed for processing. 
 *
 */

import java.util.*;

public class Parenthesis {

	public static void main(String[] args) {
		String p = "((()())())";
		
		if (isBalanced(p)){
			System.out.println(p + " is balanced!");
		} else {
			System.out.println(p + " is NOT balanced.");
		};
	}
	
	public static boolean isBalanced(String string){
		
		int rightCount = 0;
		int leftCount = 0;
		
		for (int i = 0; i < string.length(); i++){
			if (string.charAt(i) == '('){
				leftCount++;
			}
			
			if (string.charAt(i) == ')'){
				rightCount++;
			}
			
			if (rightCount > leftCount){
				return false;				
			}
			
		}
		
		return (leftCount == rightCount);

	}

}
