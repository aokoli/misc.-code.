import java.util.*;

public class Dqueue<E> extends LinkedList<E>{
	
	public E enqueHead(E element){  
		this.add(0, element);
		return element;
	}
	
	public E enqueTail(E element){
		this.add(element);
		return element;
	}
	
	public E dequeHead(){
		return this.removeFirst();
	}
	
	public E dequeTail(){
		return this.removeLast();
	}
	
	
	public E peekHead(){
		return this.peekFirst();
	}
	
	public E peekTail(){
		return this.peekLast();
	}	

}

/* TEST 
 * 
 * public static void main(String[] args) {
	
		Dqueue<Integer> test = new Dqueue<Integer>();
		test.enqueHead(1);
		test.enqueHead(2);
		test.enqueHead(3);
		test.enqueHead(5);
		
		test.enqueTail(10);
		test.enqueTail(11);
		test.enqueTail(13);
		
		test.dequeHead();
		test.dequeHead();
		
		test.dequeTail();
		
		System.out.println(test);
		System.out.println(test.peekHead());
		System.out.println(test.peekTail());

	}
 */
