package com.okolialex.alextimer.model.state;

import com.okolialex.alextimer.android.R;

/**
 * The Alarm state in the state machine. It is reached when
 * the count down has been naturally completed on the AlexTimer. 
 * 
 * @author Alexander Okoli
 *
 */
public class AlarmState implements AlexTimerState {

	AlexTimerStateMachine sm;
	
	public AlarmState(AlexTimerStateMachine sm) {
		this.sm = sm;
	}

	@Override
	public void onButtonPush() {
		sm.actionStopAlarm();		
		sm.actionResetCurrentTime();
		sm.actionResetIdleTime();
		sm.toStoppedState();
	}

	@Override
	public void onTick() {
		sm.actionStartAlarm();
		sm.actionStopClock(); 
	}

	@Override
	public int getStateId() {
		return R.string.ALARM;
	}

	@Override
	public int getButtonId() {
		return R.string.stop_button;
	}

}
