package com.okolialex.alextimer.model.state;

/**
 * 
 * The restricted view states have of their surrounding state machine.
 * This is the "context" where state-dependent activities take place.
 * Depending on the particular state of the state machine, actions are
 * invoked from this interface.
 * This is a client-specific interface in Peter Coad's terminology.
 *
 * @author Alexander Okoli
 */
public interface AlexTimerSMStateView {

	// transitions
	void toStoppedState();
	void toTimeSetState();
	void toRunningState();
	void toAlarmState();

	// actions
	void actionInit();  
	void actionResetCurrentTime();
	void actionStartClock();
	void actionStopClock();
	void actionIncIdleTime();
	void actionResetIdleTime();
	void actionDecCurrentTime();
	void actionIncCurrentTime();
	void actionUpdateEntireUI();
	void actionUpdateUITime();
	void actionStartAlarm();
	void actionStopAlarm();
}
