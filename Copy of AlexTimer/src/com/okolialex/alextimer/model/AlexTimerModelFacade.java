package com.okolialex.alextimer.model;

import com.okolialex.alextimer.common.AlexTimerUIListener;
import com.okolialex.alextimer.common.AlexTimerUIUpdateAware;

/**
 * A thin model facade. Following the Facade pattern,
 * this isolates the complexity of the model from its clients (usually the adapter).
 *
 * @author Alexander Okoli
 */

public interface AlexTimerModelFacade extends AlexTimerUIListener, AlexTimerUIUpdateAware {
	 void onStart(); 
}

