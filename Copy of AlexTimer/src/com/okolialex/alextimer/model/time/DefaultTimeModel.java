package com.okolialex.alextimer.model.time;

import static com.okolialex.alextimer.common.Constants.*;

/**
 * An implementation of the AlexTimer data model.
 * 
 * @author Alexander Okoli
 */
public class DefaultTimeModel implements TimeModel {

	private int currentTime = 0;  
	
	private int idleTime = 0;
	
	@Override
	public int getIdleTime() {
		return idleTime;	
	}

	@Override
	public void resetIdleTime() {
		idleTime = 0;
	}

	@Override
	public void incIdleTime() {
		idleTime += SEC_PER_TICK;
	}

	@Override
	public void incCurrentTime() {
		if (!isFull()) { currentTime += TIME_INC_PER_CLICK; }	
	}
	
	@Override
	public int getCurrentTime() {
		return currentTime;
	}
	
	@Override
	public boolean isFull() {
		return currentTime >= MAX_TIME_SETTABLE; 
	}

	@Override
	public void decCurrentTime() {		
		if (!isEmpty()) { currentTime -= SEC_PER_TICK; }  
	}

	@Override
	public void resetCurrentTime() {
		currentTime = 0;		
	}

	@Override
	public boolean isEmpty() {
		return currentTime <= MIN_TIMER_TIME;
	}

	@Override
	public boolean isIdleTimeMaxed() {
		return idleTime >= MAX_IDLE_TIME;
	}
}
