package com.okolialex.alextimer.model.time;

/**
 * The passive data model of time-related increments in AlexTimer.
 * It does not emit any events.
 *
 * @author Alexander Okoli
 */
public interface TimeModel {	
	public void resetIdleTime(); 
	public void incIdleTime();
	public int getIdleTime();  // Added primarily for testing.
	public boolean isIdleTimeMaxed();
	public void incCurrentTime();
	public void decCurrentTime();
	public void resetCurrentTime(); 
	public int getCurrentTime();
	public boolean isFull();
	public boolean isEmpty();
}
