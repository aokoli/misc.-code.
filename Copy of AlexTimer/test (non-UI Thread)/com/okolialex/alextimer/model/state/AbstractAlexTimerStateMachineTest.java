package com.okolialex.alextimer.model.state;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.okolialex.alextimer.android.R;
import static com.okolialex.alextimer.common.Constants.*;

import com.okolialex.alextimer.common.AlexTimerUIUpdateListener;
import com.okolialex.alextimer.model.clock.ClockModel;
import com.okolialex.alextimer.model.clock.OnTickListener;
import com.okolialex.alextimer.model.clock.RunnableScheduler;
import com.okolialex.alextimer.model.time.TimeModel;

/**
 * Testcase superclass for the AlexTimer state machine model. Tests the state
 * machine in fast-forward mode by directly triggering successive tick events
 * without the presence of a pseudo-real-time clock. Uses a single unified mock
 * object for all dependencies of the state machine model.
 *
 * @author Alexander Okoli
 * @see http://xunitpatterns.com/Testcase%20Superclass.html
 */
public abstract class AbstractAlexTimerStateMachineTest {

	private AlexTimerStateMachine model;

	private UnifiedMockDependency dependency;

	@Before
	public void setUp() throws Exception {
		dependency = new UnifiedMockDependency();
	}

	@After
	public void tearDown() {
		dependency = null;
	}

	/**
	 * Setter for dependency injection. Usually invoked by concrete testcase
	 * subclass.
	 *
	 * @param model
	 */
	protected void setModel(final AlexTimerStateMachine model) {
		this.model = model;
		if (model == null)
			return;
		this.model.setUIUpdateListener(dependency);
		this.model.actionInit();
	}

	protected UnifiedMockDependency getDependency() {
		return dependency;
	}

	/**
	 * Verifies that we're initially in the stopped state (and told the listener
	 * about it).
	 */
	@Test
	public void testPreconditions() {
		assertEquals(R.string.STOPPED, dependency.getState());
	}
	
	/**
     * Verifies the following: Time is 0, button is pressed until the maximum set-able time 
	 * is reached, 99 is displayed as current time, button is not accessible for further clicks.
	 */
	@Test
	public void testActivityScenarioIncUntilFull(){
		assertTimeEquals(0);
		assertEquals(R.string.STOPPED, dependency.getState());
		assertEquals(R.string.set_time_button, dependency.getButtonId());
		assertTrue(dependency.isButtonAccessible());
		onButtonPushRepeat(MAX_TIME_SETTABLE);
		assertTrue(dependency.isStarted());
		assertEquals(R.string.TIME_SET, dependency.getState());
		assertEquals(R.string.set_time_button, dependency.getButtonId());
		assertFalse(dependency.isButtonAccessible());
		assertTimeEquals(99);
	}
	

	/**
	 * Verifies the following scenario: Time is 0, button is pressed 2 times and released, 
	 * 2 is displayed in UI, 3 seconds elapse, count down to 0 begins, count down ends in 2 seconds,
	 * alarm is triggered, button is pressed and alarm stops.
	 */
	@Test
	public void testScenarioRun() {
		assertTimeEquals(0);
		assertEquals(R.string.STOPPED, dependency.getState());
		assertEquals(R.string.set_time_button, dependency.getButtonId());
		onButtonPushRepeat(2);  
		assertTrue(dependency.isStarted());
		assertEquals(R.string.TIME_SET, dependency.getState());
		assertEquals(R.string.set_time_button, dependency.getButtonId());
		assertTrue(dependency.isStarted());
		assertFalse(dependency.isAlarmPlaying());
		//assertTrue(dependency.isButtonAccessible());
		onTickRepeat(3);
		assertTimeEquals(2);
		idleTimeElapsed(3);
		assertEquals(R.string.RUNNING, dependency.getState());	
		assertEquals(R.string.cancel_button, dependency.getButtonId());
		assertTrue(dependency.isStarted());
		assertFalse(dependency.isAlarmPlaying());
		//assertTrue(dependency.isButtonAccessible());
		countDownTimeElapsed(2);	
		assertTimeEquals(0);
		onTickRepeat(2); // 2 ticks. 1st tick is for the final onTick in RUNNING state, the 2nd for the first onTick in ALARM state
		assertEquals(R.string.ALARM, dependency.getState());	
		assertEquals(R.string.stop_button, dependency.getButtonId());
		assertFalse(dependency.isStarted());
		assertTrue(dependency.isAlarmPlaying());
		//assertTrue(dependency.isButtonAccessible());
		assertTimeEquals(0);
		onButtonPushRepeat(1);
		assertEquals(R.string.STOPPED, dependency.getState());
		assertEquals(R.string.set_time_button, dependency.getButtonId());
		assertFalse(dependency.isStarted());
		assertFalse(dependency.isAlarmPlaying());
		//assertTrue(dependency.isButtonAccessible());
		assertTimeEquals(0);
	}

	/**
	 * Verifies the following scenario: Time is 0, button is pressed 2 times and released, 
	 * 2 is displayed in UI, 3 seconds elapse, count down to 0 begins, 1 second elapses, 1 
	 * is displayed in UI, button is pressed and count down is cancelled, 0 is displayed in the UI. 
	 */
	@Test
	public void testScenarioRunCancel() {
		assertTimeEquals(0);
		assertEquals(R.string.STOPPED, dependency.getState());
		assertEquals(R.string.set_time_button, dependency.getButtonId());
		//assertTrue(dependency.isButtonAccessible());
		onButtonPushRepeat(2);  
		assertTrue(dependency.isStarted());
		assertEquals(R.string.TIME_SET, dependency.getState());
		assertEquals(R.string.set_time_button, dependency.getButtonId());
		assertTrue(dependency.isStarted());
		assertFalse(dependency.isAlarmPlaying());
		//assertTrue(dependency.isButtonAccessible());
		onTickRepeat(3);
		assertTimeEquals(2);
		idleTimeElapsed(3);
		assertEquals(R.string.RUNNING, dependency.getState());	
		assertEquals(R.string.cancel_button, dependency.getButtonId());
		assertTrue(dependency.isStarted());
		assertFalse(dependency.isAlarmPlaying());
		//assertTrue(dependency.isButtonAccessible());
		countDownTimeElapsed(1);	
		assertTimeEquals(1);
		onButtonPushRepeat(1);  
		assertEquals(R.string.STOPPED, dependency.getState());
		assertEquals(R.string.set_time_button, dependency.getButtonId());
		//assertTrue(dependency.isButtonAccessible());
		assertTimeEquals(0);
	}

	
	/**
	 * Pushes the button a specified number of times
	 * 
	 * @param n is the number of times the button is pushed
	 */
	protected void onButtonPushRepeat(int n) {
		for (int i = 0; i < n; i++){
			model.onButtonPush();			
		}	
	}
	
	/**
	 * Increments the idle time in the model
	 * 
	 * @param t is the idle time sent to the model
	 */
	protected void idleTimeElapsed(int t) {
		for (int i = 0; i < t; i++){
			model.actionIncIdleTime();			
		}	
	}

	/**
	 * Decrements the current time in the model
	 * 
	 * @param t is the number of seconds the model should be decremented by 
	 */
	protected void countDownTimeElapsed(int t) {
		for (int i = 0; i < t; i++){
			model.actionDecCurrentTime();			
		}	
	}
	
	/**
	 * Sends the given number of tick events to the model.
	 *
	 *  @param n the number of tick events
	 */
	protected void onTickRepeat(final int n) {
		for (int i = 0; i < n; i++)
			model.onTick();
	}

	/**
	 * Checks whether the model has invoked the expected time-keeping
	 * methods on the mock object.
	 */
	protected void assertTimeEquals(final int t) {
		assertEquals(t, dependency.getTime());
	}
}

/**
 * Manually implemented mock object that unifies the three dependencies of the
 * stopwatch state machine model. The three dependencies correspond to the three
 * interfaces this mock object implements.
 *
 * @author Alexander Okoli
 */
class UnifiedMockDependency implements TimeModel, ClockModel, AlexTimerUIUpdateListener {

	private int timeValue = -1, stateId = -1, buttonNameId = -1; 

	private int currentTime = 0, idleTime = 0;

	private boolean started = false, alarmPlaying = false, buttonAccessible = true ;

	public boolean isAlarmPlaying(){
		return alarmPlaying;
	}

	public int getTime() {
		return timeValue;
	}

	public int getState() {
		return stateId;
	}
	
	public int getButtonId(){
		return buttonNameId;
	}
	
	public boolean isButtonAccessible(){
		return buttonAccessible;
	}

	public boolean isStarted() {
		return started;
	}

	@Override
	public void updateTime(final int timeValue) {
		this.timeValue = timeValue;
	}

	@Override
	public void updateState(final int stateId) {
		this.stateId = stateId;
	}

	@Override
	public void setRunnableScheduler(RunnableScheduler scheduler) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setOnTickListener(OnTickListener listener) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void start() {
		started = true;
	}

	@Override
	public void stop() {
		started = false;
	}

	@Override
	public void updateButtonName(int buttonNameId) {
		this.buttonNameId = buttonNameId;
		
	}

	@Override
	public void updateButtonAccess(boolean isFull) {
		if (isFull) { buttonAccessible = false;}
		
	}

	@Override
	public void playDefaultAlarm() {
		alarmPlaying  = true;	
	}

	@Override
	public void stopDefaultAlarm() {
		alarmPlaying  = false;	
		
	}

	public void resetIdleTime() {
		idleTime = 0;
	}

	public void incIdleTime() {
		idleTime += SEC_PER_TICK;
	}

	public void incCurrentTime() {
		if (!isFull()) { currentTime += TIME_INC_PER_CLICK; }	
	}
	
	public int getCurrentTime() {
		return currentTime;
	}
	
	public boolean isFull() {
		return currentTime >= MAX_TIME_SETTABLE; 
	}

	public void decCurrentTime() {		
		if (!isEmpty()) { currentTime -= SEC_PER_TICK; } 
	}

	public void resetCurrentTime() {
		currentTime = 0;		
	}

	public boolean isEmpty() {
		return currentTime <= MIN_TIMER_TIME;
	}

	public boolean isIdleTimeMaxed() {
		return idleTime >= MAX_IDLE_TIME;
	}

	@Override
	public int getIdleTime() {
		return idleTime;
	}

}