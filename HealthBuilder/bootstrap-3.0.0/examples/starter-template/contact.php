<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.png">

    <title>Starter Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="../../dist/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="starter-template.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>




    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Health Builder</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container">

      <div class="starter-template">
        <h1>Contact Us!</h1>
        <!-- <p class="lead">Use this document as a way to quickly start any new project.<br> All you get is this text and a mostly barebones HTML document.</p> -->
      </div>

      <center>

<div class="span12">
                                                        <div class="page-header">
                                                                <h2>Send us a message</h2>
                                                        </div>
                                                        <p>We want to hear from you! Just enter your name, email address, and message into the form below and send away.</p>

                                                        <p>All fields are required</p>

                                                        <form method="post" action="email.php">
                                                                <fieldset>
                                                                                    <div class="clearfix">
                                                                                <label for="name"><span>Name:</span></label>
                                                                                <div class="input">
                                                                                        <input tabindex="1" id="name" name="name" label="Name" type="text" value="">
                                                                                </div>
                                                                        </div>
                                                                                    <div class="clearfix">
                                                                                <label for="email"><span>Email:</span></label>
                                                                                <div class="input">
                                                                                        <input tabindex="2" id="email" name="email" label="Email" type="text" value="">
                                                                                </div>
                                                                        </div>
                                                                                    <div class="clearfix">
                                                                                <label for="message"><span>Message:</span></label>
                                                                                <div class="input">
                                                                                        <textarea tabindex="3" class="xxlarge" id="message" name="body" label="Message" rows="7"></textarea>
                                                                                </div>
                                                                        </div><br>
                                                                        <div class="actions">
                                                                                <input tabindex="4" type="submit" class="btn primary large" value="Send message">
                                                                        </div>
                                                                </fieldset>
                                                        </form>
                                                </div>
                                        </div>
                                                                        </center>

                                </div>



                        </div>
                </div>
<footer>
<div id="wrap">

      <!-- Begin page content -->
      <div class="container">
        <div class="page-header">
        </div>
        <p class="lead">All Rights Reserved Health Builder 2.0</p>
        <p>© 2011 — 2013. HealthBuilder™ and the HealthBuilder logo are registered trademarks.</p>
      </div>
    </div>

    <div id="footer">
      <div class="container">
        <p class="text-muted credit">Hosted by <a href="http://jumblethis.com">Jumble This LLC.</a></p>
      </div>
    </div>

</footer>











    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../../assets/js/jquery.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
  </body>
</html>
