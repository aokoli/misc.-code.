<?php 
include('config.php'); 
$query_parent = mysql_query("SELECT * FROM itemtype") or die("Query failed: ".mysql_error());
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="food, points, health">
    <meta name="author" content="Christina Bijayananda & Alexander Okoli">
    <link rel="shortcut icon" href="images/favicon.ico">

    <title>HealthBuilder Recipes</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="jumbotron.css" rel="stylesheet">
    <script type="text/javascript" src="js/jquery.js"></script>
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	
	<!--fancybox-->
	<link rel="stylesheet" href="fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
	<script type="text/javascript" src="fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
	
	<script type="text/javascript"> 
  
	 jQuery(document).ready(function($){
					
					
					//For dependent dropdowns
					
					$("#parent_cat").change(function() {
						$(this).after('<div id="loader"><img src="images/loading.gif" alt="loading subcategory" /></div>');
						$.get('loadsubcat.php?parent_cat=' + $(this).val(), function(data) {
							$("#sub_cat").html(data);
							$('#loader').slideUp(200, function() {
								$(this).remove();
							});
						});	
				    });
					
					$("#sub_cat").change(function() {
						$(this).after('<div id="loader"><img src="img/loading.gif" alt="loading subcategory" /></div>');
						$.get('loadsubcatsub.php?sub_cat=' + $(this).val(), function(data) {
							$("#sub_cat_2").html(data);
							$('#loader').slideUp(200, function() {
								$(this).remove();
							});
						});	
				    });
										
					
					
					
					
					//added for the dynamic display of images
					$("#single_2").fancybox({
										openEffect	: 'elastic',
										closeEffect	: 'elastic',

										helpers : {
											title : {
												type : 'inside'
											}
										}
									});
					
					
					$('#recipe').autocomplete({
						source:'suggest_recipe.php', 
						minLength:2,
						select: function( event, ui){
										var $selectedObj = ui.item;
										
										$selectedObj.attr("id",$selectedObj.id);
										$selectedObj.attr("href", $selectedObj.link);
									
										},

						success: function(data){
											   response($.map(data, function(item) {
												  //console.log(item)
												  return {
													 label: __highlight(item.label, request.term),
													 value: item.label
												  };
											   }));
											},
						open: function(){

								   $(this).removeClass("wait");
								},

						search: function(event, ui) {
									$(this).addClass("wait");
								  }
					});
					
			
	});
	
	function __highlight(s, t) {
	  var matcher = new RegExp("("+$.ui.autocomplete.escapeRegex(t)+")", "ig" );
	  return s.replace(matcher, "<strong>$1</strong>");
	}


	//This is to calculate women calories:
	
	function FigureCalorie(form, feet, inches, pounds, years) {
		
		var e = document.getElementById("genderSelection");
			TotalInches = eval(feet*12) + eval(inches)
			 Centis      = TotalInches * 2.54
			 Kilos       = pounds/2.2
			 Age         = years
		if(e.options[e.selectedIndex].value == 2){
			
			 Weight      = 655 + (9.6 * Kilos)
			 Height      = 1.7 * Centis
			 Ages        = 4.7 * Age
			 var activity = document.calorieForm.activity.options[document.calorieForm.activity.selectedIndex].value			
			 form.calcUserTotalCalorie.value = (Math.round(Weight + Height - Ages) * activity)
		}
		else{
			
			 Weight      = 66 + (13.7 * Kilos)
			 Height      = 5 * Centis
			 Ages        = 6.8 * Age
			var activity = document.calorieForm.activity.options[document.calorieForm.activity.selectedIndex].value			
			 form.calcUserTotalCalorie.value = (Math.round(Weight + Height - Ages) * activity)
		}		 
	}
	
		function getRemainingCalorie(){
			var TotalCalForUser = document.getElementById("calcUserTotalCalorie").value;
			var UserConsumedCal = document.getElementById("userConsumedCalorie").value;
			
			//alert(TotalCalForUser);
				document.getElementById("userRemainingCalorie").value = (Math.round(TotalCalForUser - UserConsumedCal))
		}
	
  </script>
	
  </head>

  <style>
  #tab-container {
    overflow:hidden;
}
</style>

  <body>
	<!-- NavBar on top // Recipe Search Nav added here! Remove when test is complete-->
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="http://gohealthbuilder.com/">Health Builder</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="http://gohealthbuilder.com/">Home</a></li>
            <li><a href="http://gohealthbuilder.com/bootstrap-3.0.0/examples/starter-template/about.html">About</a></li>
            <li><a href="http://gohealthbuilder.com/bootstrap-3.0.0/examples/starter-template/contact.html">Contact</a></li>
            <li><a href="Recipe Search/RecipeSearch.html">Recipe Search</a></li>
             
          </ul>
          <form class="navbar-form navbar-right">
            <div class="form-group">
              <input type="text" placeholder="Email" class="form-control">
            </div>
            <div class="form-group">
              <input type="password" placeholder="Password" class="form-control">
            </div>
            <button type="submit" class="btn btn-success">Sign in</button>
          </form>
        </div><!--/.navbar-collapse -->
      </div>
    </div>

	<div class="jumbotron">
      <div class="container">
        <center><h1>Health Builder</h1></center>
        <center><p>Don't bother handling you're health, we'll do it for you!</p></center>
          
              <div class="accordion" id="monogram-acc">
                  <div class="accordion-group">
                      <div class="accordion-heading">
                          <center><div class="btn btn-primary btn-lg"><a href="myRecipeIndex.php">BUILD YOUR SCHEDULE </a></div></center>
						  <div>  </div>
						  <center><div class="btn btn-primary btn-lg" data-toggle="collapse" data-parent="#monogram-acc" href="#Animals">POINTS SYSTEM</div></center>
                      </div>
                      <center><div class="accordion-body collapse" id="Animals"><br>
                      <div class="container">
    <div class="container">
  <div class="row">
    <div class="span12">

     <div class="panel-group" id="accordion">
		 <div class="panel panel-default">
			<div class="panel-heading">
		  <h4 class="panel-title">
			<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
			  Get your Points Value a Day!
			</a>
		  </h4>
		</div>
				<div id="collapseOne" class="panel-collapse collapse in">
		  <div class="panel-body">
			  <div class="btn-group" data-toggle="buttons">
					<form id="calorieForm" name="calorieForm">
					<table>
						<tr>
							<td><label for="user_lic">Height: </label> 
								<input id="user_lic" name="feet" type="number" min="3" max="8" step="1" value ="4"></input><abbr title="feet">ft</abbr>
								<input id="user_lic" type="number" name="inches" min="0" max="11" step="1" value ="0"></input><abbr title="inches">in</abbr></td></tr>
						<tr>
							<td><label for="user_lic">Weight: </label> 
								<input id="user_lic" type="number" name="pounds" min="90" max="400" step="1" value ="120"></input><abbr title="pounds">lbs</abbr></td></tr>
						<tr>
							<tr>
							<td><label for="user_lic">Years: </label> 
								<input id="user_lic" type="number" name="years" min="15" max="100" step="1" value ="20"></input><abbr title="years"></abbr></td></tr>
						<tr>
							<td><label for="user_lic">Gender: </label> 										
								<select id="genderSelection" name="gender">
									<option value="1">Male</option>
									<option value="2" selected="selected">Female</option>
								</select>
							</td></tr>
						<tr>
							<td><label for="user_lic">Activity level (choose): </label> 	
							<select name="activity">
							    <option value="1.25">Sedentary</option>
							    <option value="1.3">Lightly Exercise/Activity</option>
							    <option value="1.5" selected="selected">Moderate Exercise/Activity</option>
							    <option value="1.7">Heavy Exercise/Activity</option>
							    </select>
						</tr>
						
						
						<tr><input type="button" onclick="FigureCalorie(this.form, this.form.feet.value,this.form.inches.value,this.form.pounds.value, this.form.years.value)" name="calc" value="Determine Calories" /> <input type="reset" name="clear" value="Clear" /> <br />
    <br />
    Calories per day needed to maintain current weight: <input size="10" id="calcUserTotalCalorie" name="calcUserTotalCalorie" type="text" /></div></tr>						
					</table>
			
					</form>
			  </div>
		  </div>
		</div>
			</div>		
		</div>
		
		<div class="panel panel-default">
			<div class="panel-heading">
			  <h4 class="panel-title">
				<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
				  Quick Add Calories:
				</a>
			  </h4>
			</div>
			<div id="collapseTwo" class="panel-collapse collapse">			  
			  <div class="panel-body">
					<form>
						<fieldset>
							
						<div class="col-lg-4">
						   Calories Consumed Already: <input type="text" id="userConsumedCalorie" name="userConsumedCalorie" class="form-control" placeholder="Enter Calorie">
						</div>	
						<div class="btn btn-primary btn-lg">
						   <input type="button"  class="form-control" name="subtract" value = "Get Remaining Calorie" onclick="getRemainingCalorie()"/>
						   
						<input type="text" size="10" id="userRemainingCalorie" name="userRemainingCalorie" /> </div>
						  <legend>Choose from the list</legend>
						  
						  <form method="get">
								<label for="category">Parent Category</label>
							    <select name="parent_cat" id="parent_cat">
							        <?php while($row = mysql_fetch_array($query_parent)): ?>
							        <option value="<?php echo $row['ItemTypeID']; ?>"><?php echo $row['ItemTypeName']; ?></option>
							        <?php endwhile; ?>
							    </select>
							    <br/><br/>
							 
							    <label>Sub Category</label>
							    <select name="sub_cat" id="sub_cat"></select>
								
								<br/><br/>
							 
							    <label>Sub Category 2 </label>
							    <select name="sub_cat_2" id="sub_cat_2"></select>
								
							</form>

						  
						  
						  
						  
						  
						  
						  
					   </fieldset>
					</form>
					<form action="suggest_recipe.php" method="post">
						<div class="form-group">
						
						
						  <input type="text" id="recipe" placeholder="Find a Recipe" class="form-control">
						</div>
					</form>
						
				  </div>
			  </div>
			</div>
	
		
		
 <div class="panel panel-default">
	<div class="panel-heading">
      <h4 class="panel-title">
        <p>Blah Blah</p>
			<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
			  Click ME!! Although I have nothing to display! 
			</a>
      </h4>
	  
    </div>
	<div id="collapseSeven" class="panel-collapse collapse">
		<div class="panel-body">
          <div class="btn-group" data-toggle="buttons">                
                <div class="col-lg-4">
				<script type="text/javascript">
					// Popup window code
					function newPopup(url) {
						popupWindow = window.open(
							url,'popUpWindow','height=700,width=800,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
					}
					</script>
					<a href="JavaScript:newPopup('http://www.quackit.com/html/html_help.cfm');">Open a popup window</a>
				
				</div>
			</div>
		</div>
	</div>
	</div>
	</center>
      </div>
    </div>

	
	<div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-lg-4">
          <h2>Create.</h2>
          <p>It's as easy as clicking a button, literally. Start above to go through a small three step plan to becoming healthy. </p>
          <!-- <p><a class="btn btn-default" href="#">View details &raquo;</a></p> -->
        </div>
        <div class="col-lg-4">
          <h2>Save.</h2>
          <p>Save your plan then come back after you've finished. We're mobile accessible, so it shouldn't be a problem making another plan after your first week. Our log in feature will be coming soon. Stay Tuned! </p>
          <!-- <p><a class="btn btn-default" href="#">View details &raquo;</a></p> -->
       </div>
        <div class="col-lg-4">
          <h2>Workout.</h2>
          <p>Not only will you have a healthy diet, we've thrown in some workouts as well. This should speed up your goal to perfection.</p>
          <!-- <p><a class="btn btn-default" href="#">View details &raquo;</a></p> -->
        </div>
      </div>

      <hr>

      <footer>
        <p>&copy; Health Builder 2013</p>
      </footer>
    </div> <!-- /container -->

						<center><div class="accordion-body collapse" id="myAnimals"><br>
					

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="dist/js/slider-form.js"></script>
    <script src="assets/js/jquery.js"></script>
    <script src="dist/js/bootstrap.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script> 
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.min.js" type="text/javascript"></script> 
<script src="/bootstrap/js/bootstrap.min.js"></script> 
<script src="/js/slider_input.js"></script>
<script src="/testtyler2.php"></script>

<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/themes/base/jquery-ui.css" type="text/css" media="all" />
<link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">

  </body>

					
</html>
