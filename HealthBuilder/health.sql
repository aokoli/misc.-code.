/*
 * Queries to create mySql tables. 
 * Nema's table structure intact with additional tables/columns. 
 * We can alter as we go.
 */

create table Person (
	 PersonID		int	not null auto_increment,
	 FirstName      varchar(20) 	not null,
     LastName		varchar(20) 	not null,
	 Email			varchar(50)		not null,
	 Message		varchar(255)	,
	 primary key (PersonID)
);

create table Recipe(
	 RecipeID		int	not null auto_increment,
	 RecipeName      varchar(100) 	not null,
	 RecipeType		varchar(50) 	not null,
	 Description		varchar(255)	,
	 RecipePoints			int,	
	 primary key (RecipeID)
);
/*	alter table Recipe
	drop column Points;
	alter table Recipe
	add RecipePoints int
*/

create table WorkOut (
	 WorkOutID		int	not null auto_increment,
	 WorkOutName      varchar(100) 	not null,
     WorkOutType		varchar(50),	
	 primary key (WorkOutID)
);

create table FoodItems(
	ItemID		int not null auto_increment,
	ItemName	varchar(50)		not null,
	ItemPoint	int,
	primary key (ItemID)
);
/*
	alter table FoodItems
	drop column ItemPoint;
	alter table FoodItems
	add ItemPoints int;
	*/

create table RecipeItems(
	RecipeItemsID	int not null auto_increment,
	RecipeID	int	not null,
	ItemID		int not null,
	primary key (RecipeItemsID),
	foreign key (RecipeID) references Recipe(RecipeID),
	foreign key (ItemID) references FoodItems(ItemID)
);
