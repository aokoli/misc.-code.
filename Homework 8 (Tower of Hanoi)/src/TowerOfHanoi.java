
public class TowerOfHanoi {

	public static void main(String[] args) {
		toh('A', 'B', 'C', 4);
	}
	
	
	public static void toh (char source , char temp, char dest, int n){
		if (n==1){ 
			System.out.println("Move disk from " + source + " to " + dest);
		}else{
			toh(source, dest, temp, n-1);
			System.out.println("Move disk from " + source + " to " + dest);
			toh(temp, source, dest, n-1);
		}
	}

}
